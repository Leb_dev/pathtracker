package dev.cyberhatter.pathtracker;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.IBinder;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.preference.PreferenceManager;

import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationResult;

import java.util.Calendar;

import dev.cyberhatter.pathtracker.models.VisitedPlaceInfo;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class LocationTrackingService extends Service {
    LocationManager locationManager;
    DatabaseHelper databaseHelper;
    SharedPreferences sharedPreferences;

    @Override
    public void onCreate() {
        super.onCreate();
        startForeground(1, createNotification(this));

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        databaseHelper = new DatabaseHelper(this);
        LocationCallback locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                Location location = locationResult.getLastLocation();
                databaseHelper.addNewVisitedPlace(new VisitedPlaceInfo(
                        0,
                        location.getLatitude(),
                        location.getLongitude(),
                        Calendar.getInstance().getTimeInMillis()
                ));
            }
        };
        int period = 60000;
        if (sharedPreferences.contains("background_period")) {
            period = Integer.parseInt(sharedPreferences.getString("background_period", "60000"));
        }
        locationManager = new LocationManager(this, period, locationCallback, false);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        locationManager = null;
        super.onDestroy();
    }

    Notification createNotification(Context context) {
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        String NOTIFICATION_CHANNEL_MAIN_ID = "PathTrackerMainNotificationChannelId";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String NOTIFICATION_CHANNEL_MAIN_NAME = "PathTrackerMainNotificationChannelName";
            NotificationChannel notificationChannel =
                    new NotificationChannel(
                            NOTIFICATION_CHANNEL_MAIN_ID,
                            NOTIFICATION_CHANNEL_MAIN_NAME,
                            NotificationManager.IMPORTANCE_HIGH
                    );
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.BLUE);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(true);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        Notification notification =
                new NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_MAIN_ID)
                        .setSmallIcon(R.drawable.ic_launcher_foreground)
                        .setContentTitle("PathTracker")
                        .setContentText("The app is tracking your location in background")
                        .setAutoCancel(true)
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                        .build();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            notification.defaults =
                    notification.defaults |
                            Notification.DEFAULT_LIGHTS |
                            Notification.DEFAULT_VIBRATE |
                            Notification.DEFAULT_SOUND;
        }
        return notification;
    }
}
