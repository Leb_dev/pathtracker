package dev.cyberhatter.pathtracker.models;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class VisitedPlaceInfo {
    int id;
    double latitude;
    double longitude;
    long time;
}
