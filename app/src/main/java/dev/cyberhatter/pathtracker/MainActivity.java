package dev.cyberhatter.pathtracker;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.preference.PreferenceManager;

import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationResult;
import com.yandex.mapkit.Animation;
import com.yandex.mapkit.MapKitFactory;
import com.yandex.mapkit.geometry.Point;
import com.yandex.mapkit.geometry.Polyline;
import com.yandex.mapkit.map.CameraPosition;
import com.yandex.mapkit.map.ColoredPolylineMapObject;
import com.yandex.mapkit.mapview.MapView;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dev.cyberhatter.pathtracker.models.VisitedPlaceInfo;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_ACCESS_LOCATION = 0;
    @BindView(R.id.mapview)
    MapView mapView;
    @BindView(R.id.dayPicker)
    NumberPicker dayPicker;
    @BindView(R.id.monthPicker)
    NumberPicker monthPicker;
    @BindView(R.id.yearPicker)
    NumberPicker yearPicker;
    @BindView(R.id.datePicker)
    View datePicker;
    //Ключ Яндекс Карт
    final String DEVELOPER_KEY = "6c517c06-e3ae-4976-8f72-950495555168";
    DatabaseHelper databaseHelper;
    Calendar pickerCalendar = Calendar.getInstance();
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        MapKitFactory.setApiKey(DEVELOPER_KEY);
        MapKitFactory.initialize(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        preSetUpPickers();

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_ACCESS_LOCATION);
        } else {
            setUpLocationClient();
        }
    }

    private void preSetUpPickers() {
        pickerCalendar.set(Calendar.MILLISECOND, 0);
        pickerCalendar.set(Calendar.SECOND, 0);
        pickerCalendar.set(Calendar.MINUTE, 0);
        pickerCalendar.set(Calendar.HOUR, 0);
        yearPicker.setMinValue(1980);
        yearPicker.setMaxValue(2500);
        yearPicker.setValue(pickerCalendar.get(Calendar.YEAR));
        monthPicker.setMinValue(1);
        monthPicker.setMaxValue(12);
        monthPicker.setValue(pickerCalendar.get(Calendar.MONTH) + 1);
        dayPicker.setMinValue(1);
        dayPicker.setMaxValue(pickerCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        dayPicker.setValue(pickerCalendar.get(Calendar.DAY_OF_MONTH));
        datePicker.setVisibility(View.GONE);
        yearPicker.setOnScrollListener((numberPicker, i) -> {
            pickerCalendar.set(Calendar.YEAR, numberPicker.getValue());
            dayPicker.setMaxValue(pickerCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        });
        monthPicker.setOnScrollListener((numberPicker, i) -> {
            pickerCalendar.set(Calendar.MONTH, numberPicker.getValue());
            dayPicker.setMaxValue(pickerCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        });
        dayPicker.setOnScrollListener((numberPicker, i) -> {
            pickerCalendar.set(Calendar.DAY_OF_MONTH, numberPicker.getValue());
        });
    }

    @OnClick(R.id.switchButton)
    void switchVisibility() {
        if (datePicker.getVisibility() != View.VISIBLE) {
            datePicker.setVisibility(View.VISIBLE);
        } else {
            datePicker.setVisibility(View.GONE);
            pickerCalendar.set(Calendar.DAY_OF_MONTH, dayPicker.getValue());
            pickerCalendar.set(Calendar.MONTH, monthPicker.getValue() - 1);
            pickerCalendar.set(Calendar.YEAR, yearPicker.getValue());
            mapView.getMap().getMapObjects().clear();
            drawPath();
        }
    }

    @SuppressLint("MissingPermission")
    private void setUpLocationClient() {
        startService(new Intent(this, LocationTrackingService.class));
        databaseHelper = new DatabaseHelper(this);
        LocationCallback locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                //Добавление в БД
                Location location = locationResult.getLastLocation();
                databaseHelper.addNewVisitedPlace(new VisitedPlaceInfo(
                        0,
                        location.getLatitude(),
                        location.getLongitude(),
                        Calendar.getInstance().getTimeInMillis()
                ));
                //Перерисовка пути
                mapView.getMap().getMapObjects().clear();
                drawPath();
                //Отметка о текущем местоположении
                Point point = new Point(location.getLatitude(), location.getLongitude());
                mapView.getMap().getMapObjects().addPlacemark(point);
                //Перемещение камеры
                moveCameraToPosition(point);
            }
        };
        int period = 5000;
        if (sharedPreferences.contains("active_period")) {
            period = Integer.parseInt(sharedPreferences.getString("active_period", "5000"));
        }
        new LocationManager(this, period, locationCallback, true);
    }

    /**
     * Перемещение камеры
     *
     * @param point Новая позиция
     */
    private void moveCameraToPosition(Point point) {
        float zoom = mapView.getMap().getCameraPosition().getZoom();
        if (zoom < 10) {
            zoom = 14;
        }
        CameraPosition cameraPosition = new CameraPosition(point, zoom, 0.0f, 0.0f);
        //Анимация - плавная/линейная, длительность в секундах
        mapView.getMap().move(cameraPosition, new Animation(Animation.Type.SMOOTH, 0.150f), null);
    }

    private void drawPath() {
        ArrayList<Point> path = databaseHelper.getPath(pickerCalendar);
        //Отрисовка точки и пути
        if (path.size() > 1) {
            ColoredPolylineMapObject coloredPolylineMapObject = mapView.getMap().getMapObjects().addColoredPolyline(new Polyline(path));
            ArrayList<Integer> colors = new ArrayList<>();
            for (int i = 0; i < path.size() - 1; i++) {
                colors.add(Color.BLUE);
            }
            coloredPolylineMapObject.setColors(colors);
            coloredPolylineMapObject.setOutlineColor(Color.RED);
            coloredPolylineMapObject.setStrokeWidth(8);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        for (int res : grantResults) {
            if (res != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Permissions not granted for this app", Toast.LENGTH_LONG).show();
                return;
            }
        }
        setUpLocationClient();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
        MapKitFactory.getInstance().onStop();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();
        MapKitFactory.getInstance().onStart();
    }
}