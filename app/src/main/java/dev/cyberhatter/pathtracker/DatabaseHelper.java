package dev.cyberhatter.pathtracker;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import com.yandex.mapkit.geometry.Point;

import java.util.ArrayList;
import java.util.Calendar;

import dev.cyberhatter.pathtracker.models.VisitedPlaceInfo;

public class DatabaseHelper extends SQLiteOpenHelper {

    private final static String DATABASE_NAME = "pathTrackerData.db";
    private final static int DATABASE_VERSION = 1;
    private static final String TABLE_NAME_VISITED_PLACES = "visitedPlaces";
    private static final String KEY_VISITED_PLACES_ID = "_id";
    private static final String KEY_VISITED_PLACES_LATITUDE = "latitude";
    private static final String KEY_VISITED_PLACES_LONGITUDE = "longitude";
    private static final String KEY_VISITED_PLACES_VISIT_TIME = "visitTime";
    private SQLiteDatabase sqLiteDatabase = null;

    DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public SQLiteDatabase getWritableDatabase() {
        if (sqLiteDatabase == null) {
            return sqLiteDatabase = super.getWritableDatabase();
        } else {
            return sqLiteDatabase;
        }
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.beginTransaction();
        try {
            sqLiteDatabase.execSQL(
                    "create table " + TABLE_NAME_VISITED_PLACES +
                            "(" +
                            KEY_VISITED_PLACES_ID + " integer primary key," +
                            KEY_VISITED_PLACES_LATITUDE + " real," +
                            KEY_VISITED_PLACES_LONGITUDE + " real," +
                            KEY_VISITED_PLACES_VISIT_TIME + " bigint" +
                            ")"
            );
            sqLiteDatabase.setTransactionSuccessful();
        } finally {
            sqLiteDatabase.endTransaction();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
    }

    void addNewVisitedPlace(VisitedPlaceInfo visitedPlaceInfo) {
        SQLiteStatement sqLiteStatement = getWritableDatabase().compileStatement(
                "insert into " + TABLE_NAME_VISITED_PLACES + " (" +
                        KEY_VISITED_PLACES_LATITUDE + ", " +
                        KEY_VISITED_PLACES_LONGITUDE + ", " +
                        KEY_VISITED_PLACES_VISIT_TIME +
                        ")" +
                        "values (?, ?, ?)"
        );
        sqLiteStatement.bindDouble(1, visitedPlaceInfo.getLatitude());
        sqLiteStatement.bindDouble(2, visitedPlaceInfo.getLongitude());
        sqLiteStatement.bindLong(3, visitedPlaceInfo.getTime());
        sqLiteStatement.executeInsert();
    }

    ArrayList<Point> getPath(Calendar calendar) {
        ArrayList<Point> points = new ArrayList<>();
        Cursor cursor = getWritableDatabase().rawQuery(
                "select * from " + TABLE_NAME_VISITED_PLACES + " " +
                        "where " + KEY_VISITED_PLACES_VISIT_TIME + ">=" + calendar.getTimeInMillis() + " " +
                        "and " + KEY_VISITED_PLACES_VISIT_TIME + "<" + (calendar.getTimeInMillis() + 86400000)  + " " +
                        "order by " + KEY_VISITED_PLACES_VISIT_TIME,
                null
        );
        while (cursor.moveToNext()) {
            points.add(new Point(
                    cursor.getDouble(cursor.getColumnIndex(KEY_VISITED_PLACES_LATITUDE)),
                    cursor.getDouble(cursor.getColumnIndex(KEY_VISITED_PLACES_LONGITUDE))
            ));
        }
        cursor.close();
        return points;
    }
}
