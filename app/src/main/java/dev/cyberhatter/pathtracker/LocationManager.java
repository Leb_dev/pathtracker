package dev.cyberhatter.pathtracker;

import android.content.Context;
import android.os.Looper;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

class LocationManager {
    LocationManager(Context context, int interval, LocationCallback locationCallback, boolean highPriority) {
        LocationRequest locationRequest = buildLocationRequest(interval, highPriority);
        FusedLocationProviderClient fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context);
        fusedLocationProviderClient.requestLocationUpdates(
                locationRequest,
                locationCallback,
                Looper.myLooper()
        );
    }

    /**
     * Построение запроса позиции
     */
    private LocationRequest buildLocationRequest(int interval, boolean highPriority) {
        LocationRequest locationRequest = new LocationRequest();
        //Приоритет
        locationRequest.setPriority(highPriority ? LocationRequest.PRIORITY_HIGH_ACCURACY : LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        //Интервал обновления (мс)
        locationRequest.setInterval(interval);
        //Минимальный интервал обновления (мс)
        locationRequest.setFastestInterval(interval * 3 / 5);
        //Минимальное смещение расстояния для запроса (м)
        locationRequest.setSmallestDisplacement(highPriority ? 10 : 50);
        return locationRequest;
    }
}
